import { useHistory, Link as ReachLink } from "react-router-dom";
import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";

import { useAuth } from "../../Providers/Auth";
import jwtDecode, { JwtPayload } from "jwt-decode";
import api from "../../Services/api";
import loginImage from "../../Assets/img/loginImg.png"

import {
  useMediaQuery,
  Box,
  Button,
  FormControl,
  Text,
  useToast,
  Image,
  Heading,
  extendTheme
} from "@chakra-ui/react";
import { createBreakpoints } from '@chakra-ui/theme-tools';
import { useUser } from '../../Providers/User'

import { Flex, Stack } from "@chakra-ui/layout";
import { FaLock } from "react-icons/fa";
import { MdEmail } from "react-icons/md";

import { Input } from "../../Components/Form/Input";

interface ILogin {
  username: string;
  password: string;
}

const Login = () => {

  const breakpoints = createBreakpoints({
    sm: '320px',
    md: '425px',
    lg: '768px',
    xl: '1024px',
    '2xl': '1440px',
  })

  const theme = extendTheme({ breakpoints })
  
  const formSchema = yup.object().shape({
    email: yup
      .string()
      .required("Required Field!")
      .email("Ivalid email!"),
    password: yup
      .string()
      .required("Required Field!")
      .min(8, "Minimum 8 characters!"),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(formSchema),
  });

  const { setUserName } = useUser()
  const { setAuth } = useAuth();
  const history = useHistory();

  const toast = useToast();

  const addSuccessToast = () => {
    toast({
      description: "",
      duration: 5000,
      position: "top",
      status: "success",
      title: "Welcome to Coopers To-do list!",
    });
  };

  const addFailToast = () => {
    toast({
      description: "Wrong Email or Password!",
      duration: 5000,
      position: "top",
      status: "error",
      title: "Login failed!",
    });
  };

  const submitFunction = (data: ILogin) => {
    api
      .post("login/", data)
      .then((response) => {
        const { token } = response.data;
        const decoded = jwtDecode<any>(token);
        setAuth(token);
        setUserName(decoded.userData.name)
        window.localStorage.setItem(
          "@to-do: token",
          JSON.stringify(token)
        );
        addSuccessToast();
        history.push( "/", { user: decoded.sub });
      })
      .catch((_) => {
        addFailToast();
      });
  };
  return (
      <Box
        w={[200, 320, 320, 700, 932]}
        h={[400, 400, 400, 600, 721]}
        bg='white'
        fontFamily= "'Montserrat', sans-serif"
        fontStyle= 'normal'
        fontWeight= '500' 
        ml={['60px', '60px', '60px','-120','-254']}
        > 
           <Flex
            justifyContent='center'
           >
              <Image 
                src={loginImage}
                w={['75px','75px', 120, 210, 231]}
                h={['60px','60px', 90, 210, 231]}
                mt={[50, 50, 50, 90, 100]}/>
              <Box             
                align='left'
                mt={[50, 50,  50,  138, 138]}
            >
                <Heading 
                  fontFamily= "'Montserrat', sans-serif"
                  fontStyle= 'normal'
                  fontWeight= 'bold'
                  fontSize={[20, 20,  30, 64, 64]}
                  mb='-10px'>
                  Sign in
              </Heading>
                <Text 
                   fontFamily= "'Montserrat', sans-serif"
                   fontStyle= 'normal'
                   fontWeight= 'normal'
                   color='#42D76B' 
                   fontSize={[12, 12,  17, 48, 48]}>
                    to access your list 
                </Text>      
              </Box>
        </Flex>
          <Flex
                align="center"
                direction="column"
                position="relative"
                bottom="120px"
                zIndex="2"
              >
                <Box>
                  <form onSubmit={handleSubmit(submitFunction)}>
                    <FormControl
                      align='center'
                      padding={["1rem 2rem","1rem 2rem","1rem 2rem","0.5rem 4rem","0.5rem 4rem"]}
                      mt="32%"
                    >
                      <Stack spacing="2">
                        <Text align='left'>Email:</Text>
                        <Input
                           border='1px solid black'
                          error={errors.email}
                          icon={MdEmail}
                          placeholder="Email"
                          type="email"
                          {...register("email")}
                          h={['30px', '30px', '30px', '40px', '40px']}

                        />
                        <Text align='left'>Password:</Text>
                        <Input
                           border='1px solid black'
                          error={errors.password}
                          icon={FaLock}
                          placeholder="Password"
                          type="password"
                          {...register("password")}
                          h={['30px', '30px', '30px', '40px', '40px']}

                        />
                      </Stack>
                      <Button
                        align='center'
                        bg="#4AC959"
                        color="white"
                        mt="30px"
                        type="submit"
                        padding='0px 100px'
                        _hover={{ bg: "#19471e" }}
                      >
                        Sign In
                      </Button>
                    </FormControl>
                  </form>
                </Box>
        </Flex>
      </Box>
  
  );
};

export default Login;
