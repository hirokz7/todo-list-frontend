import NavBar from "../../Components/NavBar";
import {
  Box,
  Button,
  Heading,
  Flex,
  useMediaQuery,
  Image,
  Link,
  extendTheme,
  Text
} from "@chakra-ui/react";
import { createBreakpoints } from '@chakra-ui/theme-tools';
import "react-multi-carousel/lib/styles.css";
import "./styles.css";
import image1 from '../../Assets/img/image1.png'
import image2 from '../../Assets/img/image2.png'
import image3 from '../../Assets/img/image3.png'
import goodThingsBg from '../../Assets/img/goodThingsBg.png'
import background from "../../Assets/img/background.png"
import backgroundLarge from "../../Assets/img/backGroundLarge.png"

import tarja from "../../Assets/img/landPageTarja.png"
import goodThings from "../../Assets/img/goodThings.png"
import backGreen from '../../Assets/img/backGreen.png'
import SendForm from '../../Components/landPageForm'
import { Footer } from '../../Components/Footer'
import Carousel from "react-multi-carousel";
import NavMobile from "../../Components/NavMobile";
import Card from "../../Components/Card";
import DragAndDrop from "../../Components/DragAndDrop/DragAndDrop"

const Landpage = () => {
  console.log("teste em produção")
  const firstCard = {
    image: image1,
    text: 'Organize your daily job enhance your life performance'
  }

  const secondCard = {
    image: image2,
    text: 'Mark one activity as done makes your brain understands the power of doing.'
  }

  const thirdCard = {
    image: image3,
    text: 'Careful with missunderstanding the difference between a list of things and a list of desires.'
  }

  const breakpoints = createBreakpoints({
    sm: '320px',
    md: '425px',
    lg: '768px',
    xl: '1024px',
    '2xl': '1440px',
    '3xl': '2560'
  })

  const theme = extendTheme({ breakpoints })


  const responsive = {
    superLargeDesktop: {
        breakpoint: { max: 4000, min: 3000 },
        items: 5
    },
    desktop: {
        breakpoint: { max: 3000, min: 1024 },
        items: 3
    },
    tablet: {
        breakpoint: { max: 1024, min: 464 },
        items: 2
    },
    mobile: {
        breakpoint: { max: 464, min: 0 },
        items: 1
    }
};

const [mobileVersion] = useMediaQuery("(max-width: 500px)");
const [largeMonitor] = useMediaQuery("(min-width: 2500px)");

const back = largeMonitor ? backgroundLarge : background

const CustomDot = ({ onMove, index, onClick, active }: any) => {
  return (
    <Flex justifyContent='space-between' w={['50px','50px','70px','80px','90px']}>
      <li
        className={active ? "active" : "inactive"}
        onClick={() => onClick()}
      > 
          {
            active? 
              <Box bg='#4AC959' p={['18px','18px','20px','25px','29px']} color='red' borderRadius='100px'/> 
              : 
              <Box  bg='#C4C4C4' p={['18px','18px','20px','25px','29px']} color='red' borderRadius='100px'/>
          }
      </li>
    </Flex>
  );
};

  return (
    <Box
      overflow='auto'
    >
      {mobileVersion ? <NavMobile /> : <NavBar />}
    <main>
    <Flex
      height="100%"
      color="white"
      flexDirection="column"
      alignItems="center" >
         <Flex
          flexDir='column'
          color='black'
          ml={[null, null, '-69%', '-60%', '-65%', '-72%']}
          mt='5%'
          >


            {!mobileVersion && !largeMonitor?
            <>
              <Image 
              src={background} 
              left='0%'
              top='0%'
              position = 'absolute'
              zIndex='-1'/>
              <Image 
              src={goodThingsBg}
              left='0%'
              top='0%'
              mt={['379vw', '145vw', '145vw', '145vw', "145vw"]}
              position = 'absolute'
              zIndex='-1'/>
            </>
            :
            largeMonitor &&
            <>
              <Image 
              src={backgroundLarge} 
              left='0%'
              top='0%'
              position = 'absolute'
              zIndex='-1'/>
              <Image 
              src={goodThingsBg}
              left='35%'
              top='0%'
              mt={['379vw', '145vw', '145vw', '145vw', "145vw", "70vw"]}
              position = 'absolute'
              zIndex='-1'/>
            </>
            }
            <Heading
              fontFamily= "'Montserrat', sans-serif"
              fontStyle= 'normal'
              fontWeight= 'bold'
              fontSize={[35, null, null, 55, 64, 100]}>
                Organize
            </Heading>
            <Heading
              fontFamily= "'Montserrat', sans-serif"
              fontStyle= 'normal'
              fontWeight= 'normal'
              color='#42D76B'
              fontSize={[25, null, null, 40, 48, 65]}
              mt='-10px'>
                your daily jobs
            </Heading>
            <Text
              fontFamily= "'Montserrat', sans-serif"
              fontStyle= 'normal'
              fontWeight= '600'
              fontSize={[12, null, null,  16, 19, 30]}
              lineH='64px'
              mt='30px'
              mb='30px'>  
                The only way to get things done
            </Text>
            {!largeMonitor &&
            <Button
              variant="outline"
              bg="#4AC959"
              color="white"
              border="none"
              borderRadius="10px"
              _hover={{ bg: "#19471e" }}
              w={[100, null, null, 243, 243]}
              p={[4, null, null, 7, 7]}
              ml={['15%', 0, 0, 0, 0]} 
              fontSize={[11, null, null,19, 19]}
              outline="none"
              href="#todolist"
            >
              <Link href="#todolist">Go to To-do list</Link>
            </Button>

            }
        </Flex>    
        <Box
          height="100%"
          color="black"
          display="flex"
          flexDirection="column"
          justifyContent='flex-start'
        >   

            <DragAndDrop /> 
         
         
        {mobileVersion && 
          <>
            <Box 
            backgroundImage={tarja} 
            bgRepeat='no-repeat'
            bgPosition='initial'
            bgSize='contain'
            position= 'absolute'
            w={['100%', '100%', null, '1024px', '1440px']}
            h={['200px', '200px', null, 400, 420]}
            left={[0, 0, 0, 0, 0]}
            top={[260, 260, null, 510, 713]}
            zIndex='-2'
            >
      
              <Text
                fontFamily= "'Montserrat', sans-serif"
                fontStyle= 'normal'
                fontWeight= '500'
                color='white'  
                fontSize='8px'
                mt='13%'
                align='center'
              >
                Drag and drop to set your main priorities, check <br/> when done and create what´s new.
              </Text>
          </Box>
          <Image 
            src={backGreen}
            position = 'absolute'
            top='51%' 
            w='270px'
            h='250px'
            left='10%'
            borderRadius='10px'
            zIndex='-1'
          />
          <Image 
            src={goodThings}
            position = 'absolute'
            top='51.5%' 
            w='100px'
            h='20px'
            left='33%'
            zIndex='-1'
          />
        </>

        } 
        <Box>  
         <Flex
            mt={['50vw','50vw', '20vw','20vw','20vw','40vw']}
            ml={['0px', '0px', '30px','-20px','-140px', '100px']}
         >
            <Box
              w={['100%', '100%', '80%','70%','80%', '50%']}
              position= 'absolute'
              paddingBottom= '80px'
            >          
                <Carousel
                  responsive={responsive}
                  infinite={true}
                  containerClass="carousel-container"
                  arrows={false}
                  renderDotsOutside
                  showDots
                  autoPlay
                  className=""
                  customDot={<CustomDot/>}
                >
                  <Box className="carousel-item active">        
                    <Card
                      image={firstCard.image}
                      text={firstCard.text}
                    />
                  </Box>
                  <Box className="carousel-item active">      
                    <Card
                      image={secondCard.image}
                      text={secondCard.text}
                    />
                  </Box>
                  <Box className="carousel-item active">
                    <Card
                      image={thirdCard.image}
                      text={thirdCard.text}
                    />
                  </Box>
                </Carousel>
            </Box>

         </Flex>
        </Box>
        <Box>
         
        </Box>   
        
          <Flex
            mt={['140vw', '150vw', '70vw', '60vw', '50vw', '-6vw']}
            ml={[0, 0, 0, 0, 0, '-60vw']}
            flexDirection="column"
          >
            <SendForm />
          </Flex> 
        
        </Box> 
    </Flex>
  </main>
    <Box
      mt={['10vw','10vw','10vw','10vw','10vw']}>
      <footer>
        <Footer/>
      </footer>
    </Box>
  </Box>

  );
};

export default Landpage;
