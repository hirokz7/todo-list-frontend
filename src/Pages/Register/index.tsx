import { useHistory, Link as ReachLink } from "react-router-dom";
import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";

import api from "../../Services/api";
import login from "../../Assets/img/login.png"

import {
  Box,
  Button,
  FormControl,
  Heading,
  Link,
  Text,
  useMediaQuery,
  useToast,
  Image,
  Checkbox,
  extendTheme
} from "@chakra-ui/react";
import { createBreakpoints } from '@chakra-ui/theme-tools';

import { Input } from "../../Components/Form/Input";

import { Flex, Stack } from "@chakra-ui/layout";
import { FaUserAlt, FaLock } from "react-icons/fa";
import { MdEmail } from "react-icons/md";

import signupImage from '../../Assets/img/signupImg.png'

interface IRegister {
  name: string;
  isAdm: boolean;
  email: string;
  password: string;
}

export const Register = () => {
  const formSchema = yup.object().shape({
    name: yup
      .string()
      .required("Required Field!")
      .min(4, "Minimum 4 characters!"),
    email: yup
      .string()
      .required("Required Field!")
      .email("Email inválido!"),
    password: yup
      .string()
      .required("Required Field!")
      .min(8, "Minimum 8 characters!"),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(formSchema),
  });

  const history = useHistory();
  const toast = useToast();

  const addSuccessToast = () => {
    toast({
      description: "",
      duration: 5000,
      position: "top",
      status: "success",
      title: "Account created!",
    });
  };

  const addFailToast = () => {
    toast({
      description: "Verify your e-mail",
      duration: 5000,
      position: "top",
      status: "error",
      title: "Fail submit!",
    });
  };

  const submitFunction = (data: IRegister) => {
    api
      .post("users", data)
      .then((response) => {
        addSuccessToast();
        history.push("/");
      })
      .catch((_) => {
        addFailToast();
      });
  };


  return (
    <Box 
          w={[200, 320, 320, 700, 932]}
          h={[400, 400, 400, 600, 721]}       
          bg='white'
          fontFamily= "'Montserrat', sans-serif"
          fontStyle= 'normal'
          fontWeight= '500' 
          ml={['60px', '60px', '60px','-120','-254']}
          >
        <Flex
          justifyContent='center'>
            <Image 
                src={signupImage}
                w={['75px','75px', 100, 195, 210]}
                h={['90px','60px', 120, 220, 251]}
                mt={[30, 21, 21, 65, 70]}/>
            <Box             
                align='left'
                mt={[55, 50,  50, 133, 138]}
            >
              <Heading 
                fontFamily= "'Montserrat', sans-serif"
                fontStyle= 'normal'
                fontWeight= 'bold'
                fontSize={[20, 20, 30, 60, 64]}
                mb='-10px'>
                  Sign up
              </Heading>
              <Text 
              fontFamily= "'Montserrat', sans-serif"
              fontStyle= 'normal'
              fontWeight= 'normal'
              color='#42D76B' 
              fontSize={[12, 12, 17, 43, 48]}>
                  to create an account
              </Text>      
            </Box>
        </Flex>
        <Flex
              align="center"
              direction="column"
              position="relative"
              bottom="120px"
              zIndex="2"
            >
              <Box>
                <form onSubmit={handleSubmit(submitFunction)}>
                  <FormControl
                    align='center'
                    padding={["1rem 2rem","1rem 2rem","1rem 2rem","0.5rem 4rem","0.5rem 4rem"]}
                    mt="32%"
                  >
                    <Stack spacing="2">
                    <Input
                        border='1px solid black'
                        error={errors.name}
                        icon={FaUserAlt}
                        placeholder="Name"
                        {...register("name")}
                        h={['30px', '30px', '30px', '40px', '40px']}

                      />
                      <Input
                        border='1px solid black'
                        error={errors.email}
                        icon={MdEmail}
                        placeholder="Email"
                        type="email"
                        {...register("email")}
                        h={['30px', '30px', '30px', '40px', '40px']}

                      />
                      <Input
                        border='1px solid black'
                        error={errors.password}
                        icon={FaLock}
                        placeholder="Password"
                        type="password"
                        {...register("password")}
                        h={['30px', '30px', '30px', '40px', '40px']}

                      />
                      <Checkbox
                        fontFamily= 'Montserrat'
                        fontStyle= 'normal'
                        fontWeight= '500'
                        defaultValue= 'false'
                        error={errors.isAdm}
                        {...register("isAdm")}
                      >Are you Admin?</Checkbox>
                    </Stack>
                    <Button
                      bg="#4AC959"
                      color="white"
                      mt="3"
                      padding='0px 100px'
                      type="submit"
                      _hover={{ bg: "#19471e" }}
                    >
                      Sign Up
                    </Button>
                  </FormControl>
                </form>
              </Box>
      </Flex>
    </Box>
  );
};

export default Register;
