import {
  Menu,
  MenuButton,
  MenuList,
  MenuItem,
  Breadcrumb, 
  BreadcrumbItem,
  BreadcrumbLink,
  Flex,
  Image,
  Box,
  useDisclosure,
  Modal,
  ModalOverlay,
  ModalContent,
  Text,
  extendTheme,
  IconButton
} from "@chakra-ui/react";
import { createBreakpoints } from '@chakra-ui/theme-tools';
import { GiHamburgerMenu } from "react-icons/gi";
import Login from "../../Pages/Login"
import Register from "../../Pages/Register"

import { Link as RouteLink } from "react-router-dom";
import Logo from "../../Assets/img/logo.png";

const NavMobile = () => {
  const { isOpen: isLoginOpen , onOpen: onLoginOpen, onClose: onLoginClose } = useDisclosure()
  const { isOpen: isSignupOpen , onOpen: onSignupOpen, onClose: onSignupClose } = useDisclosure()
  
  const breakpoints = createBreakpoints({
    sm: '320px',
    md: '425px',
    lg: '768px',
    xl: '1024px',
    '2xl': '1440px',
  })

  const theme = extendTheme({ breakpoints })


  return (
    <>
   
    <Box bg="white" display="flex" justifyContent="center" padding="0.25rem"
      mt={['15px', null, null, null, 10]} >
      <RouteLink to="/">
        <Image 
          src={Logo} 
          h={['30px', null, null, null, 10]} 
          w={['100px', null, null, null, 10]} 
        />

      </RouteLink>
      <Menu autoSelect={false}>
        <MenuButton
          as={IconButton}
          colorScheme="green"
          icon={<GiHamburgerMenu />}
          color="white"
          ml={['60px', null, null, null, 10]} 
          mr={['-80px', null, null, null, 10]}
        />
        <MenuList bg="white" border="none" color="black" padding="0px">
          <MenuItem
             fontFamily= "'Poppins', sans-serif" 
             backgroundColor="black"
             color='white'
             borderTopRadius="6px" 
             onClick={onLoginOpen}           
          >
               Login
          </MenuItem>
            <MenuItem
              fontFamily= "'Poppins', sans-serif" 
              backgroundColor="#4AC959"
              color='white'
              borderBottomRadius="6px"
              onClick={onSignupOpen}
            >
              Cadastro
            </MenuItem>
        </MenuList>
      </Menu>
    </Box>
    <Modal
    isOpen={isLoginOpen}
    onClose={onLoginClose}
  > 
    <ModalOverlay/>   
          <ModalContent autoFocus={true} >
            <Text position='absolute' zIndex='1' 
                  left={[250, 250, 380, 500, 600]}
                  fontFamily= "'Montserrat', sans-serif"
                  fontStyle= 'normal'
                  fontWeight= '700'
                  top={[2, 2, 2, 4, 4]}
                  _hover={{ color: "#4AC959" }}
                  onClick={onLoginClose}>
                    close
            </Text>
            <Login/>
          </ModalContent>          
  </Modal>
  <Modal
    isOpen={isSignupOpen}
    onClose={onSignupClose}
  > 
    <ModalOverlay/>   
          <ModalContent autoFocus={true}>
            <Text position='absolute' zIndex='1' 
                  left={[250, 250, 380, 500, 600]} 
                  fontFamily= "'Montserrat', sans-serif"
                  fontStyle= 'normal'
                  fontWeight= '700'
                  top={[2, 2, 2, 4, 4]}
                  _hover={{ color: "#4AC959" }}
                  onClick={onSignupClose}>
                    close
            </Text> 
            <Register/>
          </ModalContent>          
  </Modal>
  </>
  );
};

export default NavMobile;
