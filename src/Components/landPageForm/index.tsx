import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import api from "../../Services/api";
import {
  Box,
  Button,
  FormControl,
  Text,
  useToast,
  Image,
  extendTheme
} from "@chakra-ui/react";

import { Input } from "../../Components/Form/Input";
import { Flex, Stack } from "@chakra-ui/layout";
import { FaUserAlt, FaPhoneAlt } from "react-icons/fa";
import { MdEmail, MdMessage } from "react-icons/md";
import emailIcon from '../../Assets/img/email.png'
import landPageProfile from '../../Assets/img/landPageProfile.png'
import { createBreakpoints } from '@chakra-ui/theme-tools'

interface IRegister {
  name: string;
  email: string
  telephone: string;
  message: string;
}



export const SendForm = () => {

  const breakpoints = createBreakpoints({
    sm: '320px',
    md: '425px',
    lg: '768px',
    xl: '1024px',
    '2xl': '1440px',
  })

  const theme = extendTheme({ breakpoints })
  

  const formSchema = yup.object().shape({
    name: yup
      .string()
      .required("Required field!"),
    email: yup
      .string()
      .required("Required field!")
      .email("Email invalid!"),
    phone: yup
      .string()
      .required("Required field!"),
    message: yup.string().required("Required field!"),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(formSchema),
  });

  const toast = useToast();

  const addSuccessToast = () => {
    toast({
      description: "",
      duration: 5000,
      position: "top",
      status: "success",
      title: "Message sent!",
    });
  };

  const addFailToast = () => {
    toast({
      description: "Check your e-mail",
      duration: 5000,
      position: "top",
      status: "error",
      title: "Failed to submit!",
    });
  };

  const submitFunction = (data: IRegister) => {
    console.log(data)
    api
      .post("/email", data)
      .then((response) => {
        console.log(response)
        addSuccessToast()})
      .catch((_) => {
        addFailToast();
      });
  };

  

  return (
    <Box 
      fontFamily= "'Montserrat', sans-serif"
      fontStyle= 'normal'
      fontWeight= '400'
      fontSize= {['10px', '10px', '10px', '14px', 16]}
      lineHeight= {[null, null, '14px', '17px', '20px']}
      align="center"  
    >
            <Flex
              align="center"
              direction="column"
            >
              <Image 
                src={landPageProfile}
                position='absolute' 
                width={['100px', '100px', '200px', '200px', 300]}
                mt={['-12vw','-12vw','-10vw','-7vw','-8vw', '-4.5vw']}
              />
              <Box>
                <form onSubmit={handleSubmit(submitFunction)}>
                  <FormControl
                    align="center"
                    padding={["0.5rem 0.5rem 0.5rem 0.5rem", "2rem 2rem 2rem 2rem", "2rem 2rem 2rem 2rem", "5rem 5rem 3rem 5rem",  "5rem 5rem 3rem 5rem"]}
                    boxShadow= '0px 4px 60px rgba(66, 66, 66, 0.2)'
                  >
                    <Box 
                        alignItems='left' 
                        display='flex' 
                        justifyContent='flex-start' 
                        mb='40px'
                        mt={['40px','40px','0px','0px','0px']}
                        >
                      <Image 
                        src={emailIcon} 
                        width={['100px','150px','150px','200px','200px']}
                      />
                    </Box>

                    <Stack spacing="3">                        
                      <Flex flexDir='column' textAlign='left'>
                        <Text>Your Name</Text>
                        <Input
                            border='1px solid black'
                            error={errors.name}
                            color='primary'
                            bg='blue'
                            icon={FaUserAlt}
                            placeholder="type your name here..."
                            _hover={{ bg: "#eaffed"  }}
                            {...register("name")}
                        />
                      </Flex>
                      <Flex flexDir={['column','column', 'row', 'row', 'row']}>
                        <Flex flexDir='column' textAlign='left'>
                            <Text>Email*</Text>
                            <Input
                                mr={['0px','0px','10px','10px','10px']}
                                border='1px solid black'
                                error={errors.email}
                                icon={MdEmail}
                                placeholder="example@example.com"
                                type="email"
                                _hover={{ bg: "#eaffed" }}
                                {...register("email")}
                            />
                        </Flex>
                        <Flex flexDir='column' textAlign='left'>
                            <Text>Telephone*</Text>
                            <Input
                                border='1px solid black'
                                error={errors.phone}
                                icon={FaPhoneAlt}
                                placeholder="(  ) ____-____"
                                type="telephone"
                                _hover={{ bg: "#eaffed" }}
                                {...register("phone")}
                            />
                        </Flex>
                       
                      </Flex>
                      <Flex flexDir='column' textAlign='left'>
                        <Text>Message*</Text>
                        <Input
                            border='1px solid black'
                            error={errors.confirmtelephone}
                            icon={MdMessage}
                            placeholder="Type what you want to say to us"
                            type="message"
                            _hover={{ bg: "#eaffed" }}
                            padding={["4rem 0px","0px","7rem 1rem","7rem 1rem","7rem 1rem"]}
                            {...register("message")}
                        />
                      </Flex>
                    </Stack>
                    <Button
                      bg="#4AC959"
                      color="white"
                      mt="50px"
                      type="submit"
                      padding={["0px 3rem","0px 3rem","1rem 14rem","1rem 14rem","1rem 14rem"]}
                      _hover={{ bg: "#19471e" }}
                      boxShadow= '0px 4px 60px rgba(66, 66, 66, 0.2)'
                    >
                      SEND NOW
                    </Button>
                  </FormControl>
                </form>
              </Box>
            </Flex>
    </Box>
  );
};

export default SendForm;
