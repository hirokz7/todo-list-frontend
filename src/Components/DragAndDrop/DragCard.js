import { Draggable } from 'react-beautiful-dnd';
import api from '../../Services/api';
import { useState, useEffect } from 'react';
import {
  Checkbox,
  Flex,
  extendTheme,
  Text,
  useToast,
} from "@chakra-ui/react";
import { createBreakpoints } from '@chakra-ui/theme-tools';


const DragCard = ({ title, item, index, taskConcluded, setUpdate, update }) => {

  const token = JSON.parse(localStorage.getItem("@to-do: token") || "null");

  const breakpoints = createBreakpoints({
    sm: '320px',
    md: '425px',
    lg: '768px',
    xl: '1024px',
    '2xl': '1440px',
  })

  const theme = extendTheme({ breakpoints })

  const toast = useToast();


  const addNotDeletedOrUpdatedToast = () => {
    toast({
      description: "Something went wrong!",
      duration: 5000,
      position: "top",
      status: "error",
      title: "Task not changed!",
    });
  };


  const addDeleteToast = () => {
    toast({
      description: "",
      duration: 5000,
      position: "top",
      status: "success",
      title: "Task Deleted!",
    });
  };

  const conclude = (id) => {    
    api
      .patch(`users/todo/${id}`,{
        "isConcluded": true
    }, {
    headers: { Authorization: `Bearer ${token}` },
  })
  .then((response) => {
     setUpdate([...update, 1])
  }).catch((error) => {
    addNotDeletedOrUpdatedToast();
   
  })
  return
  }

  const notConcluded = (id) =>{
    api
      .patch(`users/todo/${id}`,{
        "isConcluded": false
      }, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((response) => {
        setUpdate([...update, 1])
      }).catch((error) => {
    addNotDeletedOrUpdatedToast();
  })
  return

  }

  const deleteTask = (id) =>{
    const token = JSON.parse(localStorage.getItem("@to-do: token") || "null");
    console.log(id)
    api
    .delete(`users/todo/${id}`,{
      headers: { Authorization: `Bearer ${token}` },
    })
    .then((response) => {
      addDeleteToast()
      setUpdate([...update, 1])
  }).catch((error) => {
    addNotDeletedOrUpdatedToast() ;
  }) 
  return
  }

  
  return (
    <Draggable key={item.id} draggableId={item.id} index={index}>
      {(provided) => (
        <div
          ref={provided.innerRef}
          {...provided.draggableProps}
          {...provided.dragHandleProps}
        >
          <Flex
            lineHeight={[null, null, null, null, '24px']} 
            mb={['20px', '20x', null, null, '20px']}
            ml={['19px', '129px', '20px', '28px']}
           >
            <Flex
                maxW={[null, null, null, null, '300px']}
                w={[null, null, null, null, '300px']}
              >
                {console.log(title)}
              <Checkbox
                colorScheme='green'
                isChecked={title === 'Done' ? true : false}
                border='orange'
                onChange={(e) => {e.target.checked ? conclude(item.id): notConcluded(item.id) } }
              />
              <Text 
                ml={['15px', '15px', null, '15px', '15px']}
                fontFamily= "'Montserrat', sans-serif"
                fontStyle= 'normal'
                fontWeight= {title === 'Done' ? "bold":"normal"}
                fontSize={[null, null, null, null, '16px']}  
               >
                  {item.name}
              </Text>      
              <Text
                ml={['20px', '20px', '20px', '20px', '20px']}
                fontFamily= "'Montserrat', sans-serif"
                fontWeight= 'bold'
                fontSize={[null, null, null, null, '12px']}  
                color='#999999'
                _hover={{ color: "red" }} 
                onClick={() => deleteTask(item.id)}
                >
                delete
              </Text>       
            </Flex>
          </Flex>
        </div>
      )}
    </Draggable>
  );
};

export default DragCard;
