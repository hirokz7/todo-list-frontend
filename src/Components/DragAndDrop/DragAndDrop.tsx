import { useState, useEffect } from 'react';
import { IoIosAddCircle } from "react-icons/io";
import { DragDropContext, Droppable } from 'react-beautiful-dnd';
import DragCard from './DragCard';
import api from "../../Services/api";
import {
  Box,
  Button,
  Flex,
  Breadcrumb,
  BreadcrumbItem,
  Image,
  Input,
  extendTheme,
  Text,
  Heading,
  IconButton,
  useToast
  
} from "@chakra-ui/react";
import { createBreakpoints } from '@chakra-ui/theme-tools';
import orangeRetangle from '../../Assets/img/orangeRetangle.png'
import greenRetangle from '../../Assets/img/greenRetangle.png'
import jwtDecode from 'jwt-decode';
import { useUser } from '../../Providers/User';


const DragAndDrop = () => {
  const [ columns, setColumns] = useState([])
  const [ update, setUpdate ] = useState([])
  const [ name, setName ] = useState('')
  const [ taskConcluded, setTaskConcluded ] = useState([])
  const token = JSON.parse(localStorage.getItem("@to-do: token") || "null");
  const { userName } = useUser()


  const breakpoints = createBreakpoints({
    sm: '320px',
    md: '425px',
    lg: '768px',
    xl: '1024px',
    '2xl': '1440px',
  })

  const theme = extendTheme({ breakpoints })

  const toast = useToast();

  const addSuccessToast = () => {
    toast({
      description: "",
      duration: 5000,
      position: "top",
      status: "success",
      title: "New task created!",
    });
  };

  const addFailToast = () => {
    toast({
      description: "Something went wrong!",
      duration: 5000,
      position: "top",
      status: "error",
      title: "New task failed!",
    });
  };

  const addNotDeletedAllToast = () => {
    toast({
      description: "Something went wrong!",
      duration: 5000,
      position: "top",
      status: "error",
      title: "Tasks not deleted!",
    });
  };

  

  const addDeleteAllToast = () => {
    toast({
      description: "Deleted All!",
      duration: 5000,
      position: "top",
      status: "success",
      title: "Now your todolist is clean!",
    });
  };

  const addNotDeletedOrUpdatedToast = () => {
    toast({
      description: "Something went wrong!",
      duration: 5000,
      position: "top",
      status: "error",
      title: "Task not changed!",
    });
  };

  useEffect(() => {
    if(columns.length === 0){
      const columnsFromBackend = {
        ['todoListId']: {
          title: 'To-do',
          items: [],
        },
        ['doneListId']: {
          title: 'Done',
          items: [],
        },
      };
      
      setColumns(columnsFromBackend as any)
    }
    const token = JSON.parse(localStorage.getItem("@to-do: token") || "null");
    api
    .get('users/profile',{
      headers: { Authorization: `Bearer ${token}` },
    })
    .then((response) => {
      const columnsFromBackend = {
        ['todoListId']: {
          title: 'To-do',
          items: response.data.todolist.filter((item: any) => !item.isConcluded),
        },
        ['doneListId']: {
          title: 'Done',
          items: response.data.todolist.filter((item: any) => item.isConcluded),
        },
      };
      
      setColumns(columnsFromBackend as any)

    }).catch((error) => {
      console.log(error);
    })
  }, [update, userName])



  const onDragEnd = (result: any, columns: any, setColumns: any) => {
    if (!result.destination) return;
    const { source, destination, draggableId } = result;
    if (source.droppableId !== destination.droppableId) {
      const sourceColumn = columns[source.droppableId];
      const destColumn = columns[destination.droppableId];
      const sourceItems = [...sourceColumn.items];
      const destItems = [...destColumn.items];
      const [removed] = sourceItems.splice(source.index, 1);
      destItems.splice(destination.index, 0, removed);
      setColumns({
        ...columns,
        [source.droppableId]: {
          ...sourceColumn,
          items: sourceItems,
        },
        [destination.droppableId]: {
          ...destColumn,
          items: destItems,
        },
      });
      if(destination.droppableId === 'doneListId'){
        api
          .patch(`users/todo/${draggableId}`,{
                "isConcluded": true
            }, {
            headers: { Authorization: `Bearer ${token}` },
          })
          .then((response) => {
            setTaskConcluded([...taskConcluded, 1] as any)
          }).catch((error) => {
            addNotDeletedOrUpdatedToast();
          })
      };
      if(destination.droppableId === 'todoListId'){
        api
          .patch(`users/todo/${draggableId}`,{
                "isConcluded": false
            }, {
            headers: { Authorization: `Bearer ${token}` },
          })
          .then((response) => {
            setTaskConcluded([...taskConcluded, 1] as any)
          }).catch((error) => {
            addNotDeletedOrUpdatedToast();
          })
          
      }
    } else {
      const column = columns[source.droppableId];
      const copiedItems = [...column.items];
      const [removed] = copiedItems.splice(source.index, 1);  
      copiedItems.splice(destination.index, 0, removed);
      setColumns({
        ...columns,
        [source.droppableId]: {
          ...column,
          items: copiedItems,
        },
      });
    }
  };


  const createTask = (name: string) =>{
    const data = {
      name: name
    }
    const token = JSON.parse(localStorage.getItem("@to-do: token") || "null");
    api
    .post('users/todo', data,{
      headers: { Authorization: `Bearer ${token}` },
    })
    .then((response) => {
      addSuccessToast()
      setUpdate([...update, 1] as any)
  }).catch((_) => {
    addFailToast() ;
  })
  return
}

const deleteAllTodo = (token: string) =>{
  const decoded = jwtDecode<any>(token)
  api
  .delete(`delete_todos/${decoded.userData.id}`, {
    headers: { Authorization: `Bearer ${token}` },
  })
  .then((response) => {
    addDeleteAllToast()
    setUpdate([...update, 1] as any)
}).catch((_) => {
  addNotDeletedAllToast() ;
})
return
}


const deleteAllDone = (token: string) =>{
  const decoded = jwtDecode<any>(token)
  api
  .delete(`delete_dones/${decoded.userData.id}`, {
    headers: { Authorization: `Bearer ${token}` },
  })
  .then((response) => {
    addDeleteAllToast()
    setUpdate([...update, 1] as any)
}).catch((_) => {
  addNotDeletedAllToast() ;
})



return
}


  return (
    <DragDropContext
      onDragEnd={(result) => {onDragEnd(result, columns, setColumns)
      }}
    >
      <Box>
        <Flex
          justifyContent='space-evenly'
          flexWrap='wrap'
          w={['100%','100%','110%','110%','110%','200%']}
          ml={[0,0,0,0,0, '-300px']}
          mt={[ '40vw', '40vw', '40vw', '42vw', "50vw", "-17vw"]}
          id="todolist"
        >
          {Object.entries(columns).map(([columnId, column]: any, index): any => {
            return (
              <Droppable key={columnId} droppableId={columnId}>
                {(provided, snapshot) => (
                  column.title === 'To-do'?
                  <Flex 
                    w={['240px','240px', '260px', '330px', '390px']}
                    flexDir='column'   
                    boxShadow= '0px 4px 12px rgba(53, 46, 46, 0.198)'          
                    mb={['20px', '20px', '0px', '0px', '0px']}
                  >
                    <Image
                      src={orangeRetangle}
                      w='100%'
                      h={[ '15px', null, null, '18px', "20px"]}
                      />
                    <Flex
                      flexDir='column'
                      w='100%'
                      maxH={['250px', '250px', '300px', '400px', 600]}
                      minH={['250px', '250px', '290px', '400px', 600]}
                      background='#FFFFFF'
                      overflow='auto'
                      ref={provided.innerRef}
                      {...provided.droppableProps}
                    >
                      <Heading
                        textAlign="center" 
                        fontFamily="'Poppins', sans-serif" 
                        fontStyle= 'normal'
                        fontWeight= '600'
                        fontSize={['20px', '20px', '30px',  '38px', '40px']}  
                        mt={[null, null, null, '20px', '20px']} 
                      > 
                        {column.title}
                      </Heading>
                      <Text
                         textAlign="center" 
                         fontFamily= "'Montserrat', sans-serif"
                         fontStyle= 'normal'
                         fontWeight= 'normal'
                         fontSize={[null, null, null, '24px', '24px']}  
                         mb={[null, null, null, '20px', '20px']}
                      > 
                        Take a breath. <br/> Start doing.
                      </Text>
                      {column.items.map((item: any, index:any) => (
                        <ul>
                           <Breadcrumb>
                              <BreadcrumbItem>
                                  <DragCard 
                                    title={column.title}
                                    key={item}
                                    item={item} 
                                    index={index} 
                                    taskConcluded={taskConcluded} 
                                    update={update}
                                    setUpdate={setUpdate} />
                              </BreadcrumbItem>
                          </Breadcrumb>
                        </ul>
                      ))}
                      {provided.placeholder}
                    </Flex>
                    <Flex>
                      <IconButton  
                        aria-label='Add Icon' 
                        colorScheme='green'
                        borderRadius= '100px'
                        _hover={{ bg: "#19471e" }} 
                        icon={<IoIosAddCircle/>}
                        ml={['20px', '25px', null, '38px', '40px']} 
                        mt={['1px', '8px', '-0.5px', '9px', '12px']} 
                        onClick={() => createTask(name)}
                      />  
                      <Input 
                        placeholder="Add new tasks"
                        textAlign="left" 
                        w={['150px', '150px', '170px', '200px', '245px']} 
                        h={['28px', '28px', '28px', '30px', '35px']} 
                        mb={['10px', '10px', null, '15px', '20px']} 
                        mt={['8px', '8px', null, '15px', '15px']} 
                        ml={['4px', '4px', '4px', '4px', '5px']} 
                        fontFamily= "'Montserrat', sans-serif"
                        fontStyle= 'normal'
                        fontWeight= 'normal'
                        fontSize={[null, null, null, null, '16px']}  
                        focusBorderColor='#4AC959'
                        bg='#bebebeg'  
                        onChange={(e) => setName(e.target.value)}
                      />
                    </Flex>
                    
                    <Flex
                      justifyContent='center'
                      bg='white'
                      boxShadow= '0px 4px 12px rgba(53, 46, 46, 0.198)'   
                      >
                      <Button
                        w={['190px', '190px', '200px', '290px', 300]}
                        h={['40px', '40px', '50px', '60px', '64px']}
                        m='5%'
                        background='#000000'
                        color='white'
                        align='center'
                        fontFamily= "'Montserrat', sans-serif"
                        fontStyle= 'normal'
                        fontWeight= '600'
                        fontSize={[null, null, null, '22px', '24px']} 
                        _hover={{ bg: "#5f5f5f" }} 
                        borderRadius='10px'
                        onClick={() => deleteAllTodo(token)}
                      >
                        erase all
                      </Button>
                    </Flex>
                  </Flex>
                  :
                  <Flex 
                  h={[null, null, '300px', "510px", 725]}
                  w={['240px','240px', '260px', '330px', '390px']}

                  flexDir='column'
                  boxShadow= '0px 4px 12px rgba(53, 46, 46, 0.198)'   
                >
                    <Image 
                      src={greenRetangle}
                      w='100%'
                      h={[ '15px', null, null, '18px', "20px"]}
                    />
                    <Flex
                      flexDir='column'
                      w='100%'
                      maxH={['250px', '250px', '300px', '510px', 600]}
                      minH={['250px', '250px', '300px', '400px', 600]}                   
                      background='#FFFFFF'
                      overflow='auto'
                      ref={provided.innerRef}
                      {...provided.droppableProps}
                    >
                      <Heading
                        textAlign="center" 
                        fontFamily="'Poppins', sans-serif" 
                        fontStyle= 'normal'
                        fontWeight= '600'
                        fontSize={['20px', '20px', '30px',  '38px', '40px']}  
                        mt={[null, null, null, '20px', '20px']} 
                      > 
                        {column.title }
                      </Heading>
                      <Text
                        textAlign="center" 
                        fontFamily= "'Montserrat', sans-serif"
                        fontStyle= 'normal'
                        fontWeight= 'normal'
                        fontSize={[null, null, null, '24px', '24px']}  
                        mb={[null, null, null, '20px', '20px']}
                      > 
                        Congratulations. <br/> 
                      <Text  fontWeight= 'bold'>You have done {column.items.length} tasks</Text> 
                      </Text>
                      {column.items.map((item: any, index:any) => (
                        <ul>
                          <Breadcrumb>
                              <BreadcrumbItem>
                              <DragCard  
                                      title={column.title}
                                      key={item}
                                      item={item} 
                                      index={index} 
                                      taskConcluded={taskConcluded} 
                                      update={update}
                                      setUpdate={setUpdate} />                          
                              </BreadcrumbItem>
                          </Breadcrumb>
                        </ul>
                      ))}
                      {provided.placeholder}
                    </Flex>
                    <Flex
                      justifyContent='center'
                      bg='white'
                      boxShadow= '0px 4px 12px rgba(53, 46, 46, 0.198)'
                      >
                      <Button
                        w={['190px', '190px', '200px', '290px', 300]}
                        h={['40px', '40px', '50px', '60px', '64px']}
                        background='#000000'
                        color='white'
                        align='center'
                        m='5%'
                        fontFamily= "'Montserrat', sans-serif"
                        fontStyle= 'normal'
                        fontWeight= '600'
                        fontSize={[null, null, '20px', '22px', '24px']} 
                        _hover={{ bg: "#5f5f5f" }} 
                        borderRadius='10px'
                        onClick={() => deleteAllDone(token)}
                        
                      >
                        erase all
                      </Button>
                    </Flex>
                </Flex>
                )}
              </Droppable>
            );
          })}
        </Flex>
      </Box>
    </DragDropContext>
  );
};

export default DragAndDrop;