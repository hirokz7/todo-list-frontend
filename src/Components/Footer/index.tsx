import { Image, Flex, Text } from '@chakra-ui/react'
import footer from '../../Assets/img/footer.png'


export const Footer = () => {
    return ( 
        <>
         <Flex 
            bg={footer}
            flexDir='column' 
            justifyContent='center'
            align='center'   
        >
            <Image position='absolute' src={footer} zIndex='-1'/>
                <Flex
                    flexDir='column'
                    justifyContent='center'
                    align='center'
                >
                   <Text 
                        color='white'
                        fontFamily= 'Montserrat'
                        fontStyle= 'normal'
                        fontWeight= '600'
                        fontSize={['7px', '7px', 17, 20, 20, 30]}
                        >
                            Need help?
                    </Text>
                    <Text 
                        color='white'
                        fontFamily= 'Montserrat'
                        fontStyle= 'normal'
                        fontWeight= '600'
                        fontSize={['7px', '7px', 17, 20, 20, 30]}
                        mt='5%'
                        >
                            coopers@coopers.pro
                    </Text>
                    <Text 
                        color='white'
                        fontFamily= 'Montserrat'
                        fontStyle= 'normal'
                        fontWeight= '500'
                        fontSize={['5px', '5px', '9px', '10px', '10px', '20px']}
                        mt='8%'
                        >
                            © 2021 Coopers. All rights reserved.
                    </Text>
                </Flex>
         </Flex>
        </>
    )
}

