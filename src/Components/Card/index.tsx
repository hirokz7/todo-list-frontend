import {
  Box,
  Button,
  Text,
  Image,
  extendTheme,
  Flex,

} from "@chakra-ui/react";
import { createBreakpoints } from '@chakra-ui/theme-tools'
import sign from '../../Assets/img/cardCoperSign.png'

interface CardProps{
  image: string,
  text: string
}

export const Card = ({image, text}: CardProps) => {

  const breakpoints = createBreakpoints({
    sm: '320px',
    md: '425px',
    lg: '768px',
    xl: '1024px',
    '2xl': '1440px',
  })

  const theme = extendTheme({ breakpoints })



  return (
    <Flex
      FlexDir='column'
      fontFamily= "'Montserrat', sans-serif"
      fontStyle= 'normal'
      fontWeight= '400'
      fontSize= {[ '15px',  '15px',  '15px', '15px', 16]}
      lineHeight= {['19px', '19px','19px', '19px', '20px']}
      w= {['250px', '250px', '250px', '300px', 360]}
      h= {['300px', '300px', '300px', '400px', 430]}
      borderRadius='16px'
      bgColor='#FFFF'
      boxShadow= '0px 5px 5px rgba(66, 66, 66, 0.2)'
      
      >
        <Box
          position='absolute'
          >
          <Image 
            src={image}
            borderRadius='16px 16px 0 0'
            w= {['250px', '250px', '250px', '300px', 360]}
            h= {['150px', '150px', '150px', '190px', 200]}
          />
          <Image 
            src={sign}
            position= 'absolute'
            w= {[ '35px',   '35px',  '35px', '42px', '49px']}
            h= {['38px', '38px', '38px', '56px', '56px']}
            left= {[ '79%',  '79%', '79%', '79%', '80.49%']}
            top= {['130px','130px', '130px', '164px', '170px']}
            right= {[null, null, null, null, '61.11%']}
          />
          <Button
             fontFamily= "'Poppins', sans-serif"
             fontSize={['14px', '14px', '14px', '13px', '16px']}
             color='#9499B3'
             fontWeight='400'
             position='static'
             border='1px solid #9499B3'
             boxSizing='border-box'
             borderRadius='16px'
             w= {[ '70px',  '70px',  '70px', '70px', '86px']}
             h= '37px'
             p= '6px 12px 6px 12px'
             ml= {['20px', '20px', '20px', '20px', '30px']}
             mt= {['15px', '15px', '15px', '15px', '20px']}
             variant='outline'
          >
            function
          </Button>
          <Box align="center">
            <Text
              w= {['200px','200px', '200px', '200px', '296px']}
              mt= {['15px', '15px', '15px', '15px', '20px']}
              fontFamily= "'Montserrat', sans-serif"
              fontStyle= 'normal'
              fontWeight= '500'
              fontSize= {['12px', '12px', '12px', '12px', '18px']}
              lineHeight= '120%'
              color='#312F4F'
            >
              {text}
            </Text>
          </Box>
          <Box align="left">
            <Text
              mt= {['30px', '30px', '30px', '30px', '60px']}
              ml= {['25px', '25px', '25px', '25px', '35px']}
              fontFamily= "'Montserrat', sans-serif"
              fontStyle= 'normal'
              fontWeight= '700'
              fontSize= {['12px', '12px', '12px', '12px', '16px']}
              lineHeight='120%'
              color='#4AC959'
              _hover={{ color: "#19471e" }}
            >
             read more
            </Text>
          </Box>

        </Box>

    </Flex>
  );
};

export default Card;
