import {
  Breadcrumb,
  BreadcrumbItem,
  BreadcrumbLink,
  Flex,
  Image,
  Box,
  useDisclosure,
  Modal,
  ModalOverlay,
  ModalContent,
  Text,
  extendTheme,
  useMediaQuery
} from "@chakra-ui/react";
import { createBreakpoints } from '@chakra-ui/theme-tools';
import Logo from "../../Assets/img/logo.png";
import { Link as RouteLink } from "react-router-dom";
import Login from '../../Pages/Login'
import Register from "../../Pages/Register";

const NavBar = () => {
  const { isOpen: isLoginOpen , onOpen: onLoginOpen, onClose: onLoginClose } = useDisclosure()
  const { isOpen: isSignupOpen , onOpen: onSignupOpen, onClose: onSignupClose } = useDisclosure()

  const breakpoints = createBreakpoints({
    sm: '320px',
    md: '425px',
    lg: '768px',
    xl: '1024px',
    '2xl': '1440px',
  })

  const theme = extendTheme({ breakpoints })
  const [largeMonitor] = useMediaQuery("(min-width: 1441)") 


  return (
    <>
      <Breadcrumb mt='1%'>
        <BreadcrumbItem color="white" w="100%">
          <Flex 
            w="100%" 
            justify="space-between" 
            alignItems="center"
            ml={[null, null, '0%', '4%', '4%']}
            mr={[null, null, '0%', '4%', '4%']}
          >
            <Box padding="1rem">
              <header>
                <RouteLink to="/">
                  <Image 
                    src={Logo} 
                    alt="logo" 
                    w="40%"
                  />
                </RouteLink>          
              </header>
            </Box>
            <Box padding="1rem">
              <BreadcrumbItem  
                 padding="0px 5px"
                 color="white">
                <BreadcrumbLink 
                  fontFamily= "'Poppins', sans-serif" 
                  bg="#4AC959" 
                  padding='4px 25px'
                  borderRadius='5px'
                  _hover={{ bg: "#19471e" }} 
                  onClick={onSignupOpen}>
                  Sign up
                </BreadcrumbLink>
              </BreadcrumbItem>
              <BreadcrumbItem padding="0px 10px" color="white">
                <BreadcrumbLink 
                  fontFamily="'Poppins', sans-serif" 
                  bg="black" 
                  padding="4px 25px" 
                  borderRadius='5px'
                  _hover={{ bg: "#19471e" }} 
                  onClick={onLoginOpen}>
                  Login
                </BreadcrumbLink>
              </BreadcrumbItem>
            </Box>
          </Flex>
        </BreadcrumbItem>
      </Breadcrumb>
      <Modal
        isOpen={isLoginOpen}
        onClose={onLoginClose}
      > 
        <ModalOverlay/>   
              <ModalContent autoFocus={true} >
                <Text position='absolute' zIndex='1' 
                      left={[null, null, 380, 500, 600]}
                      fontFamily= "'Montserrat', sans-serif"
                      fontStyle= 'normal'
                      fontWeight= '700'
                      top={[null, null, 2, 4, 4]}
                      _hover={{ color: "#4AC959" }}
                      onClick={onLoginClose}>
                        close
                </Text>
                <Login/>
              </ModalContent>          
      </Modal>
      <Modal
        isOpen={isSignupOpen}
        onClose={onSignupClose}
      > 
        <ModalOverlay/>   
              <ModalContent autoFocus={true}>
                <Text position='absolute' zIndex='1' 
                      left={[null, null, 380, 500, 600]} 
                      fontFamily= "'Montserrat', sans-serif"
                      fontStyle= 'normal'
                      fontWeight= '700'
                      top={[null, null, 2, 4, 4]}
                      _hover={{ color: "#4AC959" }}
                      onClick={onSignupClose}>
                        close
                </Text> 
                <Register/>
              </ModalContent>          
      </Modal>
      
    </>
  );
};

export default NavBar;
