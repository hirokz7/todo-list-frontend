import { Container, Box, Flex } from "@chakra-ui/layout";
import { ReactNode, useEffect } from "react";
import MenuAside from "../MenuAside/index";
import api from "../../Services/api";


interface ContainerDashboardProps {
  children: ReactNode;
}
const ContainerDashboard = ({ children }: ContainerDashboardProps) => {
  useEffect(() => {
    api
    .get('users/profile')
    .then((response) => {
      console.log(response)
    })
  }, [])
  return (
    <Container
      padding="0"
      minH="100vh"
      minW="100vw"
    >
        <Box minW="100vh" minH="100vh" bgColor="black.transparent500">
          <Flex minH="100vh" justifyContent="space-between" alignItems="center">
            <MenuAside />
            {children}
          </Flex>
        </Box>  
    </Container>
  );
};
export default ContainerDashboard;
