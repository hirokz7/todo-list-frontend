import { VStack, Heading, Box, Text, Flex } from "@chakra-ui/layout";
import Logo from "../../Assets/img/logo.png";
import { ImHome3 } from "react-icons/im";
import { CgProfile } from "react-icons/cg";
import { RiLogoutBoxFill } from "react-icons/ri";
import { Link as ReachLink } from "react-router-dom";
import { Link, Image, Button } from "@chakra-ui/react";
import { useDisclosure } from "@chakra-ui/hooks";
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalCloseButton,
  ModalBody,
} from "@chakra-ui/modal";
import { useUser } from "../../Providers/User";
import { useForm } from "react-hook-form";
import avatarMock from "../../Assets/img/avatarMock.jpg"

const MenuAside = () => {
  const { userName } = useUser();
  const { isOpen, onOpen, onClose } = useDisclosure();
  const { register, handleSubmit } = useForm();
  const logOut = () => {
    localStorage.clear();
  };


  return (
    <VStack
      w="250px"
      minH="100%"
      bgColor="white"
      borderRadius="0px 15px 15px 0px"
      mr="10px"
    >
      <Flex
        flexDirection="column"
        alignItems="center"
        color="black"
        h="100vh"
        w="100%"
        fontFamily= "'Poppins', sans-serif"
        fontWeight="500"
      >
        <Flex justifyContent="center">
          <Image src={Logo} alt="Logo" h="45px" mt='10px' mb='25px'/>
        </Flex>
        <Box
          w="100px"
          h="100px"
          borderRadius="50%"
          bgImage={`url(${avatarMock})`}
          backgroundSize="contain"
        />
        <VStack>
          <Heading fontWeight="light" as="h3" fontSize="25px" m="1rem">
            Menu
          </Heading>
          <Box display="flex" w="150px" alignItems="center">
            <Text fontSize="35px" cursor="pointer">
              <ImHome3 fontSize="1rem" />
            </Text>
            <Heading
              as="p"
              fontSize="20PX"
              ml="5px"
              fontWeight="100"
              lineHeight="35px"
              _hover={{ transform: "translateY(-1px)", borderColor:"gray" }}
              transition="border 0.2s, ease 0s, transform 0.2s"
            >
              <Link as={ReachLink} to="/dashboard">
                Home
              </Link>
            </Heading>
          </Box>
        </VStack>
        <VStack>
          <Heading fontWeight="light" as="h3" fontSize="25px" m="1rem">
            General
          </Heading>
          <Box display="flex" w="150px" alignItems="center">
            <Text fontSize="35px" cursor="pointer">
              <CgProfile fontSize="1rem" />
            </Text>
            <Heading
              as="p"
              fontSize="20PX"
              ml="5px"
              fontWeight="100"
              lineHeight="35px"
              _hover={{ transform: "translateY(-1px)", borderColor:"gray" }}
              transition="border 0.2s, ease 0s, transform 0.2s"
            >
              <Link onClick={onOpen}>Perfil</Link>
              <Modal
                closeOnOverlayClick={false}
                isOpen={isOpen}
                onClose={onClose}
              >
                <ModalOverlay />
                <ModalContent display="flex" size="sm">
                  <ModalCloseButton />
                  <Box
                    display="flex"
                    flexDirection="column"
                    w="100%"
                    alignItems="center"
                    justifyContent="center"
                    padding="1rem"
                  >
                    <Box
                      w="100px"
                      h="100px"
                      borderRadius="50%"
                      bgColor="black.transparent500"
                      textAlign="center"
                      padding="5px"
                    />
                    <Box display="flex" flexDirection="column" mt="1rem">
                      <Text
                        fontFamily="rounded1C"
                        fontSize="1rem"
                        color="#440000"
                        textAlign="center"
                      >
                        {" "}
                        <b>User:</b> {userName}
                      </Text>
                      <Text
                        fontFamily="rounded1C"
                        fontSize="1rem"
                        color="#440000"
                        textAlign="center"
                      >
                       
                      </Text>
                      <Box padding="1rem" textAlign="center">
          
                      </Box>
                    </Box>
                  </Box>
                  <ModalBody pb={6}></ModalBody>
                </ModalContent>
              </Modal>
            </Heading>
          </Box>
          <Box display="flex" w="150px" alignItems="center">
            <Heading fontSize="35px" as="span" cursor="pointer">
              <RiLogoutBoxFill fontSize="1rem" />
            </Heading>
            <Heading
              as="p"
              fontSize="20PX"
              ml="5px"
              fontWeight="100"
              lineHeight="35px"
              _hover={{ transform: "translateY(-1px)", borderColor:"gray" }}
              transition="border 0.2s, ease 0s, transform 0.2s"
            >
              <Button
                backgroundColor="#4AC959"
                border="none"
                fontSize="20PX"
                ml="5px"
                fontWeight="100"
                lineHeight="35px"
                _hover={{ bg: "#19471e" }} 
                onClick={logOut}
                padding="0 10"
                margin-left="-1px"
              >
                <Link as={ReachLink} to="/">
                  Logout
                </Link>
              </Button>
            </Heading>
          </Box>
        </VStack>
      </Flex>
    </VStack>
  );
};
export default MenuAside;
