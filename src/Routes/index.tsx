import { Switch } from "react-router-dom";
import Route from "./route";
import Landpage from "../Pages/Landpage";


const Routes = () => {
  return (
    <Switch>
      <Route exact path="/" component={Landpage} />
    </Switch>
  );
};

export default Routes;
