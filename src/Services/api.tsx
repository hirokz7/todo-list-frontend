import axios from "axios";

const api = axios.create({
  baseURL: "https://coopers-todo-list.herokuapp.com/",
});

export default api;
